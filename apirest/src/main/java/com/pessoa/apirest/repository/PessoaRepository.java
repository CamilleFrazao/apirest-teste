package com.pessoa.apirest.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pessoa.apirest.model.PessoaFisica;

public interface PessoaRepository extends JpaRepository<PessoaFisica, Long> {
	PessoaFisica findById(long id);
}
