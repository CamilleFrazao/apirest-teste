package com.pessoa.apirest.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.pessoa.apirest.model.PessoaFisica;
import com.pessoa.apirest.repository.PessoaRepository;

@RestController
@RequestMapping(value="/api")
public class PessoaResource {

	@Autowired
	PessoaRepository pessoaRepository;
	
	@GetMapping("/pessoas")
	public List<PessoaFisica> listPessoas(){
		return pessoaRepository.findAll();
	}
	
	@GetMapping("/pessoa/{id}")
	public PessoaFisica pessoaPorId(long id){
		return pessoaRepository.findById(id);
	}
	
	@PostMapping("/pessoa")
	public PessoaFisica salvarPessoa(@RequestBody PessoaFisica pessoaFisica){		
		return pessoaRepository.save(pessoaFisica);
	}
	
	@DeleteMapping("/pessoa")
	public void deletePessoa(@RequestBody PessoaFisica pessoaFisica){
		pessoaRepository.delete(pessoaFisica);
	}
	
	@PutMapping("/pessoa")
	public void atualizaPessoa(@RequestBody PessoaFisica pessoaFisica){
		pessoaRepository.save(pessoaFisica);
	}
	
}
